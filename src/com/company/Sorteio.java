package com.company;

import java.util.Random;
import java.util.Scanner;

public class Sorteio extends Dado {
    private int numeroSorteado;

    public Sorteio() {
        super();
        this.numeroSorteado = numeroSorteado;
    }

    public void sortearDado(){
        Random random = new Random();
        Scanner ler = new Scanner(System.in);
        System.out.printf("Digite a quantidade de lados ");
        this.numeroSorteado = random.nextInt(ler.nextInt()+1);
        System.out.println(this.numeroSorteado);
    }

    public int getNumeroSorteado() {
        return numeroSorteado;
    }

    public void setNumeroSorteado(int numeroSorteado) {
        this.numeroSorteado = numeroSorteado;
    }
}
